const url = require('url');
const fs = require('fs')
const users = require('./userData.json');

/**
  * @description: Function is used to Get all user data from json file
  */
const getUsers = async (req, res) => { 
  const reqUrl = url.parse(req.url, true)
  const response = getUserData()  
   okResponse(res,response,"User get successfully.")
 }

 /**
  * @description: Function is used to Add user data in json file
  */
 const createUser = async (req, res) => {
   body = ''
    req.on('data',  function (chunk) {
        body += chunk;
      });
      req.on('end', function () {
        postBody = JSON.parse(body);

        if(postBody.email == ''){
           return errorMessage(res,'Email is required.')
        }

        //get the existing user data
        const existUsers = getUserData()

        //check if the email exist already
        const findExist = existUsers.find( user => user.email === postBody.email)
        if (findExist) {
            return errorMessage(res,'Email already exist.')
        }    
        //append the user data
        existUsers.push(postBody)

        //save the new user data
        saveUserData(existUsers);
        
        createResponse(res,postBody,"User added successfully.")
      })
  }

/**
  * @description: Function is used to delete user by email from json file
  */  
const deleteUser = async (req, res) => { 
    const reqUrl = url.parse(req.url, true)
    let email = reqUrl.query.email;

    //get the existing userdata
    const existUsers = getUserData()
    
    //filter the userdata to remove it
     const filterUser = existUsers.filter( user => user.email !== email )
     if ( existUsers.length === filterUser.length ) {
        return errorMessage(res,'This user email does not exist.')
     }
     //save the filtered data
    saveUserData(filterUser)
    okResponse(res,{},"User deleted successfully.")
   }

/**
  * @description: Function is used to update user data by email
  */   
const updateUser = async (req, res) => {

  const reqUrl = url.parse(req.url, true)
  if(!reqUrl.query.email || reqUrl.query.email == ''){
    return errorMessage(res,'Query param:Email is required.')
  }
   let email = reqUrl.query.email;
   body = ''
    req.on('data',  function (chunk) {
        body += chunk;
      });
      req.on('end', function () {
        postBody = JSON.parse(body);

        //get the existing user data
        const existUsers = getUserData()

        //check if the username exist or not       
        const findExist = existUsers.find( user => user.email === email)
        if (!findExist) {
            return errorMessage(res,'This user email does not exist.')
        }

        //filter the userdata
        const updateUser = existUsers.filter( user => user.email !== email )

        //push the updated data
        postBody.email = email

        updateUser.push(postBody)

        //finally save it
        saveUserData(updateUser)
        delete postBody.email;
        createResponse(res,postBody,"User data updated successfully.")
      })
}   

//get the user data from json file
const getUserData = () => {
    const jsonData = fs.readFileSync('userData.json')
    return JSON.parse(jsonData)    
}

//read the user data from json file
const saveUserData = (data) => {
    const stringifyData = JSON.stringify(data)
    fs.writeFileSync('userData.json', stringifyData)
}

 module.exports = {
    getUsers,
    createUser,
    deleteUser,
    updateUser
 }