
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../route');
const expect = chai.expect;
const validator = require('../validator')

chai.use(chaiHttp);
//Our parent block
describe('Users', () => {
    /*
  * Test the /GET route
  */
   describe('/GET users', () => {
       it('it should GET all the users', () => {
           
     });
  });

   /*
  * Test the /POST route
  */
 describe('/POST user', () => {
    let email = 'j.p.solanki01@gmail.com';

    it("Email required", ()=> {
      expect(validator.isEmpty(email)).to.be.true
    })

    it("Email should be valid", ()=> {
      expect(validator.isValidEmail(email)).to.be.true
    })

    let firstName = 'jaipal'
    it("FirstName Required", ()=> {
      expect(validator.isEmpty(firstName)).to.be.true
    })

});

   /*
  * Test the /DELETE route
  */
 describe('/DELETE user', () => {
  let email = 'j.p.solanki01@gmail.com';
  it("Email required for delete operation", ()=> {
    expect(validator.isEmpty(email)).to.be.true
  })
  it("Email should be valid", ()=> {
    expect(validator.isValidEmail(email)).to.be.true
  })
});

  /*
  * Test the /PUT route
  */
describe('/PUT user', () => {
  let email = 'j.p.solanki01@gmail.com';
  it("Email required for update operation", ()=> {
    expect(validator.isEmpty(email)).to.be.true
  })
  it("Email should be valid", ()=> {
    expect(validator.isValidEmail(email)).to.be.true
  })
});

})
