successResponse = function (res,data, message) {
    res.setHeader('content-Type', 'Application/json');
    res.end(JSON.stringify({"message":message,"data":data})) 
    return res
  }
  
errResponse = function (res,message) {
    res.setHeader('content-Type', 'Application/json');
    res.end(JSON.stringify({"message":message})) 
    return res
  }

okResponse = function (res, data, message) {
    res.statusCode = 200;
    if (!message) {
      message = "";
    }
    return successResponse(res,data, message);
  }

createResponse = function (res, data, message) {
    res.statusCode = 201;
    if (!message) {
      message = "";
    }
    return successResponse(res,data, message);
  }  

errorMessage = function (res,message) {
    res.statusCode = 400;
    if (!message) {
      message = "";
    }
    return errResponse(res,message);
}