exports.isNumValid = function(address) {
    if (address >= 70){
        return false
    } else if (address <= 10) {
        return false
    } else{
        return true
    }
}

exports.isEmpty = function(data) {
    if(data !== null && data !== '') {
        return true
     }else{
         return false
     }
}

exports.isValidEmail = function(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
} 